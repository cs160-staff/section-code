package com.example.noon.cuckoo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //The only thing this activity does is start a service.
        Intent i = new Intent(this, TimeWatcherService.class);
        startService(i);

        //Or start the fast service.
        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), FastTimeWatcherService.class);
                //we call getBaseContext instead of 'this' since this is not in the main
                //activity but rather in an onclick.
                startService(i);
                Toast.makeText(MainActivity.this, "Cuckoo! Sent message to watch.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
