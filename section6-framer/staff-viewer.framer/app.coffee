# Set up mask layer
mask = new Layer width:390, height:410, backgroundColor:"#fff"
mask.center()

window.onresize = ->
	mask.center()
	
mask.shadowY = 3
mask.shadowBlur = 15
mask.shadowColor = "rgba(0,0,0,0.2)"

# This imports all the layers for "staff" into staffLayers2

staffLayers2 = Framer.Importer.load "imported/staff"
staff_keys = Object.keys(staffLayers2)
for i in staff_keys
	staffLayers2[i].y = -180
# Set background
bg = new BackgroundLayer 
	backgroundColor: "#7DDD11"

# Create PageComponent
page = new PageComponent 
	width: 300
	height: 150
	scrollVertical: false
	borderRadius: 6
page.center()	



# Create layers in a for-loop
i = 0
for j in staff_keys
	layer = new Layer 
		superLayer: page.content
		width: 150
		height: 150
		image: staffLayers2[j].image
		backgroundColor: "#fff"
		borderRadius: 6
		opacity: 0.3
		x: 160 * i
		i++
	
# Staging
page.snapToNextPage()
page.currentPage.opacity = 1

# Update pages
page.on "change:currentPage", ->
	page.previousPage.animate 
		properties:
			opacity: 0.3
			scale: 0.8
		time: 0.4
		
	page.currentPage.animate 
		properties:
			opacity: 1
			scale: 1
		time: 0.4
	