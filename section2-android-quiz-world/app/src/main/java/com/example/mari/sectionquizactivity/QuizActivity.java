package com.example.mari.sectionquizactivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class QuizActivity extends Activity {

    private int index;
    private TextView mQuestion;
    private Button mTrueButton;
    private Button mFalseButton;
    private Button mNextButton;
    private Question[] mQuestions = new Question[] {
        new Question("Shiba Inus are cute", true),
        new Question("Crossroads has tasty food", false)
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        mTrueButton = (Button)findViewById(R.id.true_button);
        mFalseButton = (Button)findViewById(R.id.false_button);
        mNextButton = (Button)findViewById(R.id.next_button);
        mQuestion = (TextView)findViewById(R.id.question);
        index = 0;

        mQuestion.setText(mQuestions[index].getmQuestion());

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = (index + 1) % mQuestions.length;
                mQuestion.setText(mQuestions[index].getmQuestion());
            }
        });

        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });
    }

    private void checkAnswer(boolean answer) {
        boolean correct;
        int messageResId;
        correct = (answer == mQuestions[index].ismIsTrue());

        if (correct) {
            messageResId = R.string.correct;
        } else {
            messageResId = R.string.incorrect;
        }

        Toast.makeText(QuizActivity.this, messageResId, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
