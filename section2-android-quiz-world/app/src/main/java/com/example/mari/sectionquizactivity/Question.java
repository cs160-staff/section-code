package com.example.mari.sectionquizactivity;

/**
 * Created by Mari on 9/2/14.
 */
public class Question {
    private String mQuestion;
    private boolean mIsTrue;

    public Question(String question, boolean isTrue) {
        mQuestion = question;
        mIsTrue = isTrue;
    }

    public String getmQuestion() {
        return mQuestion;
    }

    public void setmQuestion(String mQuestion) {
        this.mQuestion = mQuestion;
    }

    public boolean ismIsTrue() {
        return mIsTrue;
    }

    public void setmIsTrue(boolean mIsTrue) {
        this.mIsTrue = mIsTrue;
    }
}
